# Lucy Pilot Issue Tracking

### How to report an issue
 1. Click Issues tab
 2. Create a new issue with title, description, screenshot or video recording if possible.
 3. You can tag people by doing @{PersonName} in the comment.
 
### What is Projects tab for
 1. You can move the card along from column to column.
 2. Easy to track the progress of all issues
 3. Filter by assignees or labels
 4. When we have a new issue created, it will be put in Backlog column automatically.
 5. When we close an issue, the card will be automatically moved to Done column.
 
 
